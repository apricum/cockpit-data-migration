export type Dictionary<Key, Value> = { [key: string]: Value }
export type AnyObject = Dictionary<string, any>
