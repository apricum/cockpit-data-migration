// Metadata

export interface Metadata {
	_id: string
	_by: string
	_mby?: string
	_created: number
	_modified?: number
}

export const cockpitMetadataKeys = ["_id", "_by", "_mby", "_created", "_modified"] as (keyof Metadata)[]

// Links

export interface CollectionLink {
	_id: string
	link: string
	display: string
}

export interface CollectionLinkOptions {
	link: string,
	display: string
	multiple: boolean
	limit: boolean
}

export interface RepeatedCollectionLink {
	field: {
		type: string
		name: string
		label: string
		options?: CollectionLinkOptions
	},
	value: CollectionLink
}

// Images

export interface Image {
	path: string
	meta?: {
		title?: string
		asset?: string
	}
}

export interface ImageLink {
	field: any
	value: Image
}

export interface CaptionLink {
	field: any
	value: string
}

export type CaptionedImageLink = ImageLink | CaptionLink
