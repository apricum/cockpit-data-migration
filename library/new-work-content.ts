import { Metadata, Image, ImageLink, CaptionedImageLink } from "./cockpit.ts"
import { ImageAlignment } from "./legacy-work-content.ts"

// Model Dependencies

export enum TextContentFormat {
	Text = "Text",
	Heading = "Heading",
	Quote = "Quote"
}

// Models

export interface WorkContentText extends Metadata {
	identifierItem: string
	identifierParent: string
	format: TextContentFormat
	textContent?: string
}

export interface WorkContentImages extends Metadata {
	identifierItem: string
	identifierParent: string
	imageContents: Image[]
	imageAlignment: ImageAlignment
}

export interface WorkContentVideoEmbed extends Metadata {
	identifierItem: string
	identifierParent: string
	videoCode: string
	videoAspectValue?: string
}

export interface WorkContentTitleCase extends Metadata {
	identifierItem: string
	identifierParent: string
	headingContent: string
	subContent?: string
	imageContent?: Image
}

export type AnyWorkContent = WorkContentText | WorkContentImages | WorkContentVideoEmbed | WorkContentTitleCase
