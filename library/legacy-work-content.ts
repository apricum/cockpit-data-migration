import { Metadata, ImageLink, CaptionedImageLink } from "./cockpit.ts"

export enum LegacyWorkContentForm {
	Heading = "Heading",
	ImageTitle = "Image Title",
	Images = "Images",
	Text = "Text",
	Quote = "Quote",
	Video = "Video"
}

export enum ImageAlignment {
	Default = "Default",
	RowsOnly = "Rows Only"
}

export interface LegacyWorkContent extends Metadata {
	identifierItem: string
	identifierParent: string
	form: LegacyWorkContentForm
	textContent?: string
	subTextContent?: string
	imageContents: CaptionedImageLink[]
	imageAlignment?: ImageAlignment
	videoCode?: string
	videoAspectValue?: string
}
