import { CollectionLink, RepeatedCollectionLink, Metadata, ImageLink, Image } from "./cockpit.ts"

export interface Work extends Metadata {
	display: boolean
	name: string
	slug: string
	description: string
	event?: CollectionLink
	titleImage?: Image
	teaserImage?: Image
	teaserImageCentered?: Image
	blocks: RepeatedCollectionLink[]
}
