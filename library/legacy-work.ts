import { CollectionLink, Metadata, Image, ImageLink } from "./cockpit.ts"

export interface LegacyWork extends Metadata {
	display: boolean
	name: string
	slug: string
	description: string
	event?: CollectionLink
	titleImage?: Image
	teaserImage?: Image
	teaserImageCentered?: Image
	blocks: CollectionLink[]
}
