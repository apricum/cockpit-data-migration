import { AnyObject, Dictionary } from "../library/types.ts"
import { Metadata, cockpitMetadataKeys, Image, ImageLink, CaptionedImageLink, CollectionLink, RepeatedCollectionLink, CollectionLinkOptions } from "../library/cockpit.ts"
import { LegacyWork } from "../library/legacy-work.ts"
import { Work } from "../library/new-work.ts"
import { LegacyWorkContent, LegacyWorkContentForm, ImageAlignment } from "../library/legacy-work-content.ts"
import {
	AnyWorkContent,
	TextContentFormat,
	WorkContentText,
	WorkContentImages,
	WorkContentVideoEmbed,
	WorkContentTitleCase
} from "../library/new-work-content.ts"

// Mappers (Meta & Base Data)

function mappedMetadata(contentBlock: AnyObject): Metadata {
	return cockpitMetadataKeys.reduce((object: AnyObject, key) => {
		object[key] = contentBlock[key]
		return object
	}, {}) as Metadata
}

function mappedContentBlock(contentBlock: LegacyWorkContent) {
	return {
		...mappedMetadata(contentBlock),
		identifierItem: contentBlock.identifierItem,
		identifierParent: contentBlock.identifierParent
	}
}

// Mappers (Types)

function mappedTextContentBlock(contentBlock: LegacyWorkContent): WorkContentText {
	return {
		...mappedContentBlock(contentBlock),
		format: TextContentFormat.Text,
		textContent: contentBlock.textContent
	}
}

function mappedHeadingContentBlock(contentBlock: LegacyWorkContent): WorkContentText {
	return {
		...mappedContentBlock(contentBlock),
		format: TextContentFormat.Heading,
		textContent: contentBlock.textContent
	}
}

function mappedQuoteContentBlock(contentBlock: LegacyWorkContent): WorkContentText {
	return {
		...mappedContentBlock(contentBlock),
		format: TextContentFormat.Quote,
		textContent: contentBlock.textContent
	}
}

function mappedGalleryImageLinks(links: CaptionedImageLink[]): Image[] {
	const mappedLinks: Image[] = []

	for (let index = 0; index < links.length; index++) {
		const currentLink = links[index]
		const nextLink = links[index + 1] as CaptionedImageLink | undefined

		if (currentLink.field.type !== "image") {
			continue
		}

		const linkedImage = currentLink.value as Image
		const mappedImage: Image = {
			...linkedImage
		}

		if (nextLink?.field.type === "text") {
			const caption = nextLink.value as string

			mappedImage.meta = {
				title: caption
			}
		}

		mappedLinks.push(mappedImage)
	}

	return mappedLinks
}

function mappedImagesContentBlock(contentBlock: LegacyWorkContent): WorkContentImages {
	return {
		...mappedContentBlock(contentBlock),
		imageContents: mappedGalleryImageLinks(contentBlock.imageContents),
		imageAlignment: contentBlock.imageAlignment ?? ImageAlignment.Default
	}
}

function mappedVideoEmbedContentBlock(contentBlock: LegacyWorkContent): WorkContentVideoEmbed | undefined {
	if (!contentBlock.videoCode) {
		return undefined
	}

	return {
		...mappedContentBlock(contentBlock),
		videoCode: contentBlock.videoCode,
		videoAspectValue: contentBlock.videoAspectValue
	}
}

function mappedTitleCaseContentBlock(contentBlock: LegacyWorkContent): WorkContentTitleCase | undefined {
	if (!contentBlock.textContent) {
		return undefined
	}

	const firstImageLink = contentBlock.imageContents.filter(imageContent => {
		return typeof imageContent.value === "object"
	})[0] as ImageLink | undefined

	if (!firstImageLink) {
		return undefined
	}

	return {
		...mappedContentBlock(contentBlock),
		headingContent: contentBlock.textContent,
		subContent: contentBlock.subTextContent,
		imageContent: firstImageLink?.value
	}
}

function mappedSingleImage(image: Image|any[]|undefined): Image|undefined {
	if (typeof image === "undefined") {
		return undefined
	}

	if (Array.isArray(image)) {
		return undefined
	}

	return image
}

// Mappers (Master)

export interface MappedWorkContent {
	collection: string
	value: AnyWorkContent
}

export interface MappedWork {
	collection: string
	value: Work
}

export function mappedContentBlocks(contentBlocks: LegacyWorkContent[]): MappedWorkContent[] {
	contentBlocks.forEach(contentBlock => {
		if (contentBlock.form === LegacyWorkContentForm.Text && contentBlock.imageContents.length > 0) {
			contentBlock.form = LegacyWorkContentForm.Images
		}
	})

	const mappedData = contentBlocks.map(contentBlock => {
		switch (contentBlock.form) {
			case LegacyWorkContentForm.Text:
				return {
					collection: "work_content_text",
					value: mappedTextContentBlock(contentBlock)
				}
			case LegacyWorkContentForm.Heading:
				return {
					collection: "work_content_text",
					value: mappedHeadingContentBlock(contentBlock)
				}
			case LegacyWorkContentForm.Quote:
				return {
					collection: "work_content_text",
					value: mappedQuoteContentBlock(contentBlock)
				}
			case LegacyWorkContentForm.Images:
				return {
					collection: "work_content_images",
					value: mappedImagesContentBlock(contentBlock)
				}
			case LegacyWorkContentForm.Video:
				return {
					collection: "work_content_video_embed",
					value: mappedVideoEmbedContentBlock(contentBlock)
				}
			case LegacyWorkContentForm.ImageTitle:
				return {
					collection: "work_content_title_case",
					value: mappedTitleCaseContentBlock(contentBlock)
				}
			default:
				return undefined
		}
	})

	return mappedData.filter(data => {
		return data && data.value
	}) as MappedWorkContent[]
}

type CollectionLinkProperties = [name: string, label: string]

const collectionLinkPropertiesByCollection: Dictionary<string, CollectionLinkProperties> = {
	work_content_text: ["text", "Text"],
	work_content_images: ["images", "Images"],
	work_content_video_embed: ["video_embed", "Video Embed"],
	work_content_title_case: ["title_case", "Title Case"]

}

export function mappedWork(work: LegacyWork[], mappedWorkContents: MappedWorkContent[]): Work[] {
	const workContentById = mappedWorkContents.reduce((dictionary: Dictionary<string, MappedWorkContent>, workContent) => {
		dictionary[workContent.value._id] = workContent
		return dictionary
	}, {})

	return work.map(legacyWork => {
		const work: Work = {
			...mappedMetadata(legacyWork),
			display: legacyWork.display,
			name: legacyWork.name,
			slug: legacyWork.slug,
			description: legacyWork.description,
			event: legacyWork.event,
			titleImage: mappedSingleImage(legacyWork.titleImage),
			teaserImage: mappedSingleImage(legacyWork.teaserImage),
			teaserImageCentered: mappedSingleImage(legacyWork.teaserImageCentered),
			blocks: []
		}

		if (work.event) {
			work.event.link = "life"
		}

		work.blocks = legacyWork.blocks.map(block => {
			const collection = workContentById[block._id].collection
			const [name, label] = collectionLinkPropertiesByCollection[collection]

			const link: RepeatedCollectionLink = {
				field: {
					type: "collectionlink",
					name: name,
					label: label,
					options: {
						link: collection,
						display: block.display,
						multiple: false,
						limit: false
					}
				},
				value: {
					_id: block._id,
					link: collection,
					display: block.display
				}
			}

			return link
		})

		return work
	})
}
