import { AnyObject } from "../library/types.ts"

export function encodedObject(object: AnyObject): string {
	return JSON.stringify(object, undefined, "\t")
}

export function encodedCSV(collection: AnyObject[]): string {
	const lines = collection.map((entry, index) => {
		return `${index + 1}, "${JSON.stringify(entry).replace(/"/g, '""')}"`
	})

	return lines.join("\n")
}
