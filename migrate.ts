import { Dictionary } from "./library/types.ts"
import { encodedCSV, encodedObject } from "./functions/coding.ts"
import { LegacyWork } from "./library/legacy-work.ts"
import { LegacyWorkContent, LegacyWorkContentForm, ImageAlignment } from "./library/legacy-work-content.ts"
import { AnyWorkContent } from "./library/new-work-content.ts"
import { mappedWork, mappedContentBlocks } from "./functions/mapping.ts"

const { run, readTextFile, writeTextFile } = Deno

function paths(basePath: string): { jsonPath: string; csvPath: string } {
	const jsonPath = basePath + ".json"
	const csvPath = basePath + ".csv"

	return { jsonPath, csvPath }
}

async function main() {
	const workPath = "./data/input/work.collection.json"
	const contentBlockPath = "./data/input/contentblock.collection.json"

	const workData = JSON.parse(await readTextFile(workPath)) as LegacyWork[]
	const contentBlockData = JSON.parse(await readTextFile(contentBlockPath)) as LegacyWorkContent[]

	console.log(`Read work data with ${workData.length} entries.`)
	console.log(`Read content block data with ${contentBlockData.length} entries.`)

	const mappedWorkContentBlocks = mappedContentBlocks(contentBlockData)
	const work = mappedWork(workData, mappedWorkContentBlocks)

	const workContentBlocksByCollection = mappedWorkContentBlocks.reduce((dictionary: Dictionary<string, AnyWorkContent[]>, mappedContentBlock) => {
		if (!dictionary[mappedContentBlock.collection]) {
			dictionary[mappedContentBlock.collection] = []
		}

		dictionary[mappedContentBlock.collection].push(mappedContentBlock.value)
		return dictionary
	}, {})

	console.log(`Processed work data, produced ${work.length} entries.`)
	console.log(
		`Processed content blocks, produced ${mappedWorkContentBlocks.length} entries total, collection keys '${Object.keys(
			workContentBlocksByCollection
		).join(", ")}'.`
	)

	const removalProcess = run({ cmd: ["rm", "./data/output/*"] })
	removalProcess.close()

	const { jsonPath, csvPath } = paths("./data/output/work.collection")
	await writeTextFile(jsonPath, encodedObject(work))
	await writeTextFile(csvPath, encodedCSV(work))

	for (const collection in workContentBlocksByCollection) {
		const { jsonPath, csvPath } = paths(`./data/output/${collection}.collection`)

		await writeTextFile(jsonPath, encodedObject(workContentBlocksByCollection[collection]))
		await writeTextFile(csvPath, encodedCSV(workContentBlocksByCollection[collection]))
	}
}

await main()
